#include <kdl_trajectories/kdl_trajectories.hpp>
#include <kdl_trajectories/PublishTraj.h>
#include <kdl_trajectories/TrajProperties.h>


int main (int  argc, char** argv)
{
    TrajectoryGenerator trajectory;

    KDL::Frame X_curr_;
    std::string csv_file_name = "trajectories/arc.csv";
    trajectory.Load(csv_file_name);
    trajectory.Build(X_curr_, true);

    Eigen::Affine3d X_traj = trajectory.Pos();
    Eigen::Matrix<double,6,1> Xd_traj = trajectory.Vel();
    Eigen::Matrix<double,6,1> Xdd_traj = trajectory.Acc();
    double duration = trajectory.Duration();

    kdl_trajectories::TrajProperties traj_properties_;
    traj_properties_.play_traj_ = true;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.play_traj_ = false;
    traj_properties_.jogging_ = true;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.jogging_ = false;
    traj_properties_.gain_tunning_ = true;
    traj_properties_.move_ = true;
    traj_properties_.index_ = 0;
    traj_properties_.amplitude = 0.1;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index_ = 1;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index_ = 2;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index_ = 3;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index_ = 4;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index_ = 5;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.gain_tunning_ = false;
    traj_properties_.move_ = false;
    trajectory.updateTrajectory(traj_properties_,0.001);
    trajectory.publishTrajectory();
    trajectory.DirectionOfMotion(0,0.01);
}